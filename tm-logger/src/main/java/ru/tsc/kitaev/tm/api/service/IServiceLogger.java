package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IServiceLogger {

    void log(@NotNull final String yaml);

}
