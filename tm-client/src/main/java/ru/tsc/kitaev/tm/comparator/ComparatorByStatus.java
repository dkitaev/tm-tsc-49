package ru.tsc.kitaev.tm.comparator;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.entity.IHasStatus;

import java.util.Comparator;

@NoArgsConstructor
public final class ComparatorByStatus implements Comparator<IHasStatus> {

    @NotNull
    private final static ComparatorByStatus INSTANCE = new ComparatorByStatus();

    @NotNull
    public static ComparatorByStatus getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable final IHasStatus o1, @Nullable final IHasStatus o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getStatus().compareTo(o2.getStatus());
    }

}
